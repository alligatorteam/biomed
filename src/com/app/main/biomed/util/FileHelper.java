package com.app.main.biomed.util;

import android.content.Context;
import android.util.Log;

import com.app.main.biomed.list.FileItem;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sebastien on 14/05/2014.
 */
public class FileHelper {
    static Context context;
    static String fileFormat = "yyyy-MM-dd HH:mm:ss";
    static String humanFormatDate = "dd/MM/yyyy";
    static String humanFormatTime = "HH:mm:ss";

    public static void initFileHelper(Context c) {
        context = c;
    }

    public static String formatForFile(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(fileFormat);
        return dateFormat.format(date);
    }

    public static Date decodeFromFileName(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(fileFormat);
        try {
            date = dateFormat.parse(dateString);
        }
        catch(ParseException e) {
            Log.e("Date parse", e.getMessage());
        }
        return date;
    }

    public static String formatForHuman(String fileDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(humanFormatDate);
        SimpleDateFormat timeFormat = new SimpleDateFormat(humanFormatTime);
        return "Le " + dateFormat.format(decodeFromFileName(fileDateString)) +
                " à " + timeFormat.format(decodeFromFileName(fileDateString));
    }

    public static void createFile(JSONArray datas) {
        String filename = formatForFile(new Date()) + ".json";
        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(datas.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<FileItem> getAllFiles() {
        String[] files = context.fileList();
        ArrayList<FileItem> array = new ArrayList<FileItem>();
        for(String file : files) {
            array.add(new FileItem(file, formatForHuman(file)));
        }
        return array;
    }

    public static JSONArray getContent(String fileName) {
        JSONArray jsonArray = new JSONArray();


        try {
            FileInputStream in = new FileInputStream(getFile(fileName));
            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String retour;
            while ((retour = bufferedReader.readLine()) != null) {
                sb.append(retour);
            }
            jsonArray = new JSONArray(sb.toString());
        }
        catch(IOException e) {
            Log.e("File", e.getMessage());
        }
        catch(JSONException e) {
            Log.e("File", e.getMessage());
        }
        return jsonArray;
    }

    public static Boolean deleteFile(String filename) {
        return context.deleteFile(filename);
    }

    public static File getFile(String filename) {
        return context.getFileStreamPath(filename);
    }
}
