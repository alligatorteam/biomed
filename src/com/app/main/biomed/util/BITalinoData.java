package com.app.main.biomed.util;

import com.bitalino.util.SensorDataConverter;

import org.json.JSONException;
import org.json.JSONObject;


public class BITalinoData {
    private double seq, LUX, EMG, EDA, ECG, ACC, ABAT;

    public BITalinoData(double seq, double LUX, double EMG, double EDA, double ECG, double ACC, double ABAT) {
        this.seq = seq;
        this.LUX = LUX;
        this.EMG = EMG;
        this.EDA = EDA;
        this.ECG = ECG;
        this.ACC = ACC;
        this.ABAT = ABAT;
    }


    public double getSeq() { return this.seq; }

    public double getLUX() { return this.LUX; }

    public double getEMG() {
        return this.EMG;
    }

    public double getEDA() {
        return this.EDA;
    }

    public double getECG() {
        return this.ECG;
    }

    public double getACC() {
        return this.ACC;
    }

    public double getABAT() {
        return this.ABAT;
    }

    public JSONObject toJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("seq", seq);

            JSONObject data = new JSONObject();
            data.put("EMG", EMG);
            data.put("EDA", EDA);
            data.put("ECG", ECG);
            data.put("ACC", ACC);
            data.put("ABAT", ABAT);

            json.put("data", data);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}