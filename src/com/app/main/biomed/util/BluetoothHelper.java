package com.app.main.biomed.util;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Sebastien on 14/05/2014.
 */
public class BluetoothHelper {
    public static BluetoothAdapter bluetoothAdapter = null;
    private static Set<BluetoothDevice> pairedDevices;
    private static BluetoothSocket blueSocket;
    private static BluetoothDevice blueDevice;
    private static UUID uuid;
    private static boolean wasActived = false;
    private static final String bluetoothTAG = "Bluetooth";

    private static Bundle b;

    /**
     * initBluetooth : Initialisation de l'adapter
     */
    public static Boolean initBluetooth(Activity activity, Bundle savedIntanceState) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null) {
            Log.e(bluetoothTAG, "Appareil non compatible");
            return false;
        }

        b = savedIntanceState;
        return on(activity);
    }

    /**
     * on : Activation du Bluetooth
     */
    public static Boolean on(Activity activity) {
        Log.d(bluetoothTAG, "Activation du Bluetooth...");
        if(bluetoothAdapter.isEnabled()) {
            wasActived = true;
            Log.d(bluetoothTAG, "Bluetooth déjà activé...");
            return true;
        }
        else {
            activity.startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
            if(bluetoothAdapter.isEnabled()) {
                Log.d(bluetoothTAG, "Bluetooth activé.");
                return true;
            }
            Log.e(bluetoothTAG, "Erreur lors de l'activation.");
            return false;
        }
    }

    /**
     * off : Désactivation du Bluetooth
     */
    public static void off() {
        Log.d(bluetoothTAG, "Désactivation du Bluetooth en cours...");
        if(!wasActived) {
            bluetoothAdapter.disable();
            Log.d(bluetoothTAG, "Bluetooth désactivé.");
        }
    }

    /**
     * Récupère la liste des appareils pairés sous forme d'array list
     *  afin de pouvoir directement adapter dans un listview
     * @return Liste des périphériques Bluetooth pairés
     */
    public static ArrayList<BluetoothDevice> getPairedDevices() {
        pairedDevices = bluetoothAdapter.getBondedDevices();
        ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        for(BluetoothDevice bd : pairedDevices) {
            list.add(bd);
        }
        return list;
    }

    /**
     * connectDevice : Permet de pairer les périphériques
     * @param device : Périphérique à pairer
     * @return le socket de communication
     */
    public BluetoothSocket connectDevice(BluetoothDevice device) {
        BluetoothSocket temp = null;
        blueDevice = device;
        /*try {
            temp = device.createInsecureRfcommSocketToServiceRecord(uuid);
        } catch(Exception e) {
            Log.e(TAG, e.getMessage());
        }*/
        return temp;
    }

    public BluetoothSocket run() {
        bluetoothAdapter.cancelDiscovery();
        try {
            blueSocket.connect();
        } catch (Exception e) {
            Log.e(bluetoothTAG, e.getMessage());
            cancel();
            return null;
        }
        return blueSocket;
    }

    public void cancel() {
        try {
            blueSocket.close();
        } catch(IOException e) {
            Log.e(bluetoothTAG, e.getMessage());
        }
    }
}
