package com.app.main.biomed;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.main.biomed.fragment.AcquiFragment;
import com.app.main.biomed.fragment.ConfigFragment;
import com.app.main.biomed.fragment.ConnexionFragment;
import com.app.main.biomed.fragment.HistoFragment;
import com.app.main.biomed.list.NavDrawerItem;
import com.app.main.biomed.list.NavDrawerListAdapter;
import com.app.main.biomed.fragment.ProposFragment;
import com.app.main.biomed.util.BluetoothHelper;
import com.app.main.biomed.util.FileHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {
    // TAG de la Main Activity
    private static final String TAG = "MAIN_BIOMED";
    // View
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter viewAdapter;
    // Préférences de l'application //
    public static SharedPreferences preferences;
    public static final String PREFERENCES_TAG = "biomed";
    public static final String RATE_TAG = "rate";
    public static final String EMG_TAG = "emg";
    public static final String EDA_TAG = "eda";
    public static final String ECG_TAG = "ecg";
    public static final String ACC_TAG = "acc";
    public static final String LUX_TAG = "lux";
    public static final String ABAT_TAG = "abat";

    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"initialisation de l'application");
        setContentView(R.layout.activity_main);

        // initialisation des préférences //
        preferences = getSharedPreferences(PREFERENCES_TAG,MODE_PRIVATE);

        // Initialisation de la vue
        initView(savedInstanceState);

        // initialisation du bluetooth
        BluetoothHelper.initBluetooth(this, savedInstanceState);

        // Sauvegarde du context
        context = getApplicationContext();

        // Initialisation du FileHelper
        FileHelper.initFileHelper(getApplicationContext());
    }

    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new AcquiFragment();
                break;

            case 1:
                fragment = new ConfigFragment();
                break;

            case 2:
                fragment = new ConnexionFragment();
                break;

            case 3:
                fragment = new HistoFragment();
                break;

            case 4:
                fragment = new ProposFragment();
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(R.string.app_name);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /********
     * VIEW *
     ********/
    public void initView(Bundle bundle) {
        // Récupération du titre
        mTitle = mDrawerTitle = getTitle();
        // Charge les menus
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // Charge les icones
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        // Récupère les éléments
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        // Associe les icones aux menus et libère la mémoire
        navDrawerItems = new ArrayList<NavDrawerItem>();
        for(int i = 0; i < navMenuTitles.length; i++) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));
        }
        navMenuIcons.recycle();

        // Création de l'évènement de slide
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // Lie la partie slidable aux menus
        viewAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(viewAdapter);

        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, // Icone de menu
                R.string.title_menu, // Nom quand le menu est ouvert
                R.string.app_name // Nom quand le menu est fermé
        )
        {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(R.string.app_name);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(R.string.title_menu);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        // Ombre sur le menu
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        if (bundle == null) {
            // on first time display view for first nav item
            displayView(2);
        }
    }

    public void toggleDrawer(){
        if(mDrawerLayout.isDrawerOpen(mDrawerList)){
            mDrawerLayout.closeDrawer(mDrawerList);
        }else{
            mDrawerLayout.openDrawer(mDrawerList);
        }
    }
}