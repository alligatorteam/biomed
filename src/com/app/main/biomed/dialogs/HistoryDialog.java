package com.app.main.biomed.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.app.main.biomed.R;
import com.app.main.biomed.fragment.AcquiFragment;
import com.app.main.biomed.fragment.HistoFragment;
import com.app.main.biomed.util.FileHelper;

import org.json.JSONArray;

import java.io.File;

/**
 * Created by Sebastien on 15/05/2014.
 */
public class HistoryDialog extends DialogFragment {
    private String fileName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Action");

        Bundle mArgs = getArguments();
        fileName = mArgs.getString("file");

        builder.setItems(R.array.history_responses, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int choix) {
                switch (choix) {
                    case 0: // Afficher
                        Bundle args = new Bundle();
                        args.putString("file", fileName);

                        Fragment aquisition = new AcquiFragment();
                        aquisition.setArguments(args);

                        FragmentManager f = getFragmentManager();
                        f.beginTransaction().replace(R.id.frame_container, aquisition).commit();
                        break;
                    case 1: // Partager
                        File file = FileHelper.getFile(fileName);
                        Uri uri = Uri.fromFile(file);

                        Intent shareData = new Intent();
                        shareData.setAction(Intent.ACTION_SEND);
                        shareData.putExtra(Intent.EXTRA_STREAM, uri);
                        shareData.setType("*/*");
                        startActivity(shareData.createChooser(shareData, "Email:"));
                        break;
                    case 2: // Supprimer
                        FileHelper.deleteFile(fileName);
                        break;
                    default:
                        break;
                }
                // The 'which' argument contains the index position
                // of the selected item
            }
        });

        return builder.create();
    }
}
