package com.app.main.biomed.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.main.biomed.MainActivity;
import com.app.main.biomed.R;

public class ProposFragment extends Fragment {
    // TAG du Fragment //
    private static final String TAG = "PROPOS_FRAGMENT";

    public ProposFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG,"initialisation du Fragment Propos en cours...");
        View rootView = inflater.inflate(R.layout.fragment_propos, container, false);

        RelativeLayout main_layout = (RelativeLayout) rootView.findViewById(R.id.main_propos);

        LinearLayout linear_layout = (LinearLayout) rootView.findViewById(R.id.linear_propos);

        main_layout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                ((MainActivity)getActivity()).toggleDrawer();
            }
        });

        linear_layout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                ((MainActivity)getActivity()).toggleDrawer();
            }
        });

        Log.d(TAG,"instanciation du Fragment Propos correctement effectuée");
        return rootView;
    }
}
