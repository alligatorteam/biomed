package com.app.main.biomed.fragment;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.main.biomed.MainActivity;
import com.app.main.biomed.R;
import com.app.main.biomed.util.BITalinoData;
import com.app.main.biomed.util.BluetoothHelper;
import com.app.main.biomed.util.FileHelper;
import com.bitalino.comm.BITalinoDevice;
import com.bitalino.comm.BITalinoException;
import com.bitalino.comm.BITalinoFrame;
import com.bitalino.util.SensorDataConverter;
import com.jjoe64.graphview.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.UUID;

public class AcquiFragment extends Fragment {
    private String deviceAdress;
    public AcquiFragment(){}
    // Sauvegarde du device
    public static BluetoothDevice device;
    public static BluetoothSocket socket;
    public static TextView log;

    private GraphView EMG;
    private GraphView EDA;
    private GraphView ECG;
    private GraphView ACC;
    private GraphView LUX;

    private GraphViewSeries EMGserie;
    private GraphViewSeries EDAserie;
    private GraphViewSeries ECGserie;
    private GraphViewSeries ACCserie;
    private GraphViewSeries LUXserie;

    private AsyncTask task;
    private ArrayList<BITalinoData> BITalinoTotalDatas;

    // Boutons //
    private Button save_button = null;
    private Button stop_button = null;
    private Button start_button = null;

    private int count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_acqui, container, false);

        //stop_button = (Button) rootView.findViewById(R.id.btn_stop_acqui);
        //stop_button.setOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View view) {

            //}
        //});
        //start_button = (Button) rootView.findViewById(R.id.btn_start_acqui);
        //start_button.setOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View view) {

            //}
        //});
        save_button = (Button) rootView.findViewById(R.id.btn_save_acqui);
        save_button.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   Log.d("Task", "Stoped");
                   task.cancel(true);
                   JSONArray array = new JSONArray(BITalinoTotalDatas);
                   FileHelper.createFile(array);
               }
        });

        Bundle args = getArguments();
        if(getArguments() != null&& getArguments().containsKey("file")) {
            String file = getArguments().getString("file");
            JSONArray datas = FileHelper.getContent(file);

            EMGserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
            EDAserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
            ECGserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
            ACCserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
            LUXserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
            for(int i = 0; i < datas.length(); i++) {
                try {
                    JSONObject json = datas.getJSONObject(i);
                    String test = json.getString("bla");
                    Log.d("Content", test);
                }
                catch (JSONException e) {
                    Log.e("Json exception", e.getMessage());
                }
            }

            EMG = new LineGraphView(this.getActivity(), "");
            EMG.addSeries(EMGserie);
            EMG.setViewPort(0, 10);
            EMG.setScrollable(true);
            EMG.setScalable(true);

            EDA = new LineGraphView(this.getActivity(), "");
            EDA.addSeries(EDAserie);
            EDA.setViewPort(0, 10);
            EDA.setScrollable(true);
            EDA.setScalable(true);

            ECG = new LineGraphView(this.getActivity(), "");
            ECG.addSeries(ECGserie);
            ECG.setViewPort(0, 10);
            ECG.setScrollable(true);
            ECG.setScalable(true);

            ACC = new LineGraphView(this.getActivity(), "");
            ACC.addSeries(ACCserie);
            ACC.setViewPort(0, 10);
            ACC.setScrollable(true);
            ACC.setScalable(true);

            LUX = new LineGraphView(this.getActivity(), "");
            LUX.addSeries(LUXserie);
            LUX.setViewPort(0, 10);
            LUX.setScrollable(true);
            LUX.setScalable(true);

            LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.emg_graph);
            layout.addView(EMG);

            LinearLayout layout1 = (LinearLayout) rootView.findViewById(R.id.eda_graph);
            layout1.addView(EDA);

            LinearLayout layout2 = (LinearLayout) rootView.findViewById(R.id.ecg_graph);
            layout2.addView(ECG);

            LinearLayout layout3 = (LinearLayout) rootView.findViewById(R.id.acc_graph);
            layout3.addView(ACC);

            LinearLayout layout4 = (LinearLayout) rootView.findViewById(R.id.lux_graph);
            layout4.addView(LUX);
        }
        else {
            deviceAdress = MainActivity.preferences.getString("device", "");
            if(deviceAdress != "") {
                Log.d("Device", deviceAdress);;
                //TODO affiche save
                //TODO affiche cancel
                log = (TextView) rootView.findViewById(R.id.acqui_log);

                EMGserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
                EDAserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
                ECGserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
                ACCserie = new GraphViewSeries(new GraphView.GraphViewData[] {});
                LUXserie = new GraphViewSeries(new GraphView.GraphViewData[] {});

                EMG = new LineGraphView(this.getActivity(), "");
                EMG.addSeries(EMGserie);
                EMG.setViewPort(0, 10);
                EMG.setScrollable(true);
                EMG.setScalable(true);

                EDA = new LineGraphView(this.getActivity(), "");
                EDA.addSeries(EDAserie);
                EDA.setViewPort(0, 10);
                EDA.setScrollable(true);
                EDA.setScalable(true);

                ECG = new LineGraphView(this.getActivity(), "");
                ECG.addSeries(ECGserie);
                ECG.setViewPort(0, 10);
                ECG.setScrollable(true);
                ECG.setScalable(true);

                ACC = new LineGraphView(this.getActivity(), "");
                ACC.addSeries(ACCserie);
                ACC.setViewPort(0, 10);
                ACC.setScrollable(true);
                ACC.setScalable(true);

                LUX = new LineGraphView(this.getActivity(), "");
                LUX.addSeries(LUXserie);
                LUX.setViewPort(0, 10);
                LUX.setScrollable(true);
                LUX.setScalable(true);

                LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.emg_graph);
                layout.addView(EMG);

                LinearLayout layout1 = (LinearLayout) rootView.findViewById(R.id.eda_graph);
                layout1.addView(EDA);

                LinearLayout layout2 = (LinearLayout) rootView.findViewById(R.id.ecg_graph);
                layout2.addView(ECG);

                LinearLayout layout3 = (LinearLayout) rootView.findViewById(R.id.acc_graph);
                layout3.addView(ACC);

                LinearLayout layout4 = (LinearLayout) rootView.findViewById(R.id.lux_graph);
                layout4.addView(LUX);

                BITalinoTotalDatas = new ArrayList<BITalinoData>();
                task = new TestAsyncTask().execute();
            }
            else {
                // Pas de Bluetooth affilié, renvoyer sur fragment connexion //
            }
        }
        return rootView;
    }

    public void appening (ArrayList<BITalinoData> data) {
        count++;
        final int fCount = count;
        final ArrayList<BITalinoData> BITdatas = data;
        ((MainActivity)getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(BITalinoData BITdata: BITdatas) {
                    int cur = 1 * fCount;
                    final int i = cur;
                    final BITalinoData data = BITdata;
                    EMGserie.appendData(new GraphViewDataInterface() {

                        @Override
                        public double getX() {
                            return data.getSeq() * i;
                        }

                        @Override
                        public double getY() {
                            return data.getEMG();
                        }
                    }, true, 86400000);
                    EDAserie.appendData(new GraphViewDataInterface() {
                        @Override
                        public double getX() {
                            return data.getSeq() * i;
                        }

                        @Override
                        public double getY() {
                            return data.getEDA();
                        }
                    }, true, 86400000);
                    ECGserie.appendData(new GraphViewDataInterface() {
                        @Override
                        public double getX() {
                            return data.getSeq() * i;
                        }

                        @Override
                        public double getY() {
                            return data.getECG();
                        }
                    }, true, 86400000);
                    ACCserie.appendData(new GraphViewDataInterface() {
                        @Override
                        public double getX() {
                            return data.getSeq() * i;
                        }

                        @Override
                        public double getY() {
                            return data.getACC();
                        }
                    }, true, 86400000);
                    LUXserie.appendData(new GraphViewDataInterface() {
                        @Override
                        public double getX() {
                            return data.getSeq() * i;
                        }

                        @Override
                        public double getY() {
                            return data.getLUX();
                        }
                    }, true, 86400000);
                    cur++;
                    BITalinoTotalDatas.add(BITdata);
                }
            }
        });
        Log.d("data", "data");
    }

    private class TestAsyncTask extends AsyncTask<Void, String, Void> {
        //private TextView tvLog = (TextView) findViewById(R.id.log);
        private BluetoothDevice dev = null;
        private BluetoothSocket sock = null;
        private InputStream is = null;
        private OutputStream os = null;
        private BITalinoDevice bitalino;

        @Override
        protected Void doInBackground(Void... paramses) {
            try {
                // Let's get the remote Bluetooth device
                final String remoteDevice = deviceAdress;

                final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                dev = btAdapter.getRemoteDevice(remoteDevice);

                /*
                 * Establish Bluetooth connection
                 *
                 * Because discovery is a heavyweight procedure for the Bluetooth adapter,
                 * this method should always be called before attempting to connect to a
                 * remote device with connect(). Discovery is not managed by the Activity,
                 * but is run as a system service, so an application should always call
                 * cancel discovery even if it did not directly request a discovery, just to
                 * be sure. If Bluetooth state is not STATE_ON, this API will return false.
                 *
                 * see
                 * http://developer.android.com/reference/android/bluetooth/BluetoothAdapter
                 * .html#cancelDiscovery()
                 */
                btAdapter.cancelDiscovery();

                sock = dev.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                sock.connect();

                bitalino = new BITalinoDevice(100, new int[]{0, 1, 2, 3, 4, 5});
                publishProgress("Connexion à [" + remoteDevice + "]..");
                bitalino.open(sock.getInputStream(), sock.getOutputStream());
                publishProgress("Connecté.");

                // get BITalino version
                publishProgress("Version: " + bitalino.version());

                // start acquisition on predefined analog channels
                bitalino.start();

                // read until task is stopped
                int counter = 0;
                while (counter < 100) {
                    final int numberOfSamplesToRead = 100;
                    BITalinoFrame[] frames = bitalino.read(numberOfSamplesToRead);
                    // present data in screen
                    ArrayList<BITalinoData> biTalinoDatas = new ArrayList<BITalinoData>();
                    for (BITalinoFrame frame : frames) {
                        Log.i("seq", Integer.toString(frame.getSequence()));
                        BITalinoData data = new BITalinoData(
                                frame.getSequence(),
                                SensorDataConverter.scaleEMG(0, frame.getAnalog(0)),
                                SensorDataConverter.scaleEDA(1, frame.getAnalog(1)),
                                SensorDataConverter.scaleLuminosity(2, frame.getAnalog(2)),
                                SensorDataConverter.scaleECG(3, frame.getAnalog(3)),
                                SensorDataConverter.scaleAccelerometer(4, frame.getAnalog(4)),
                                frame.getAnalog(5)
                        );
                        biTalinoDatas.add(data);
                    }
                    appening(biTalinoDatas);
                    if (isCancelled()) break;
                    counter++;
                }


                // trigger digital outputs
                // int[] digital = { 1, 1, 1, 1 };
                // device.trigger(digital);
            } catch (Exception e) {
                Log.e("Error", "There was an error.", e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            log.setText(values[0]);
        }

        @Override
        protected void onCancelled() {
            // stop acquisition and close bluetooth connection
            try {
                bitalino.stop();
                publishProgress("BITalino est arrêté");
                sock.close();
            } catch (Exception e) {
                Log.e("Error", "There was an error.", e);
            }
        }

    }
}
