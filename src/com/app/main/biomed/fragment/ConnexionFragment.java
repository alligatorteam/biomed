package com.app.main.biomed.fragment;

import android.app.FragmentManager;
import android.app.ListFragment;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.main.biomed.R;
import com.app.main.biomed.list.BluetoothListAdapter;
import com.app.main.biomed.util.BluetoothHelper;

import java.util.ArrayList;

public class ConnexionFragment extends ListFragment {
    public static FragmentManager fragmentManager;

    public ConnexionFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_connexion, container, false);
        if(BluetoothHelper.initBluetooth(this.getActivity(), savedInstanceState)) {
            fragmentManager = getFragmentManager();
            // Bluetooth OK
            populateList(BluetoothHelper.getPairedDevices());
        }
        return rootView;
    }

    private void populateList(ArrayList<BluetoothDevice> devices){
        BluetoothListAdapter listAdapter = new BluetoothListAdapter(this.getActivity(), R.layout.list_item_bluetooth, devices);
        this.setListAdapter(listAdapter);
    }
}