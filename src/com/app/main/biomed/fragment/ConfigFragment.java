package com.app.main.biomed.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.main.biomed.MainActivity;
import com.app.main.biomed.R;

public class ConfigFragment extends Fragment {
    // TAG du Fragment //
    private static final String TAG = "CONFIG_FRAGMENT";
    // Spinner Fréquence //
    private Spinner sample_rate = null;
    // Boutons //
    private Button save_button = null;
    private Button cancel_button = null;
    // CheckBoxes //
    private CheckBox EMGCheck = null;
    private CheckBox EDACheck = null;
    private CheckBox ECGCheck = null;
    private CheckBox ACCCheck = null;
    private CheckBox LUXCheck = null;
    private CheckBox ABATCheck = null;
    // Boolean Associés //
    private Boolean emg_state = null;
    private Boolean eda_state = null;
    private Boolean ecg_state = null;
    private Boolean acc_state = null;
    private Boolean lux_state = null;
    private Boolean abat_state = null;
    // Variables Preferences //
    private Integer rateValue = null;

    public ConfigFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG,"initialisation du Fragment Configuration en cours...");
        View rootView = inflater.inflate(R.layout.fragment_config, container, false);

        // Valorisation des Vues //
        initView(rootView);

        // Initialisation des Listeners //
        initListeners();

        // Récupération des Préférences //
        initConf();

        // Initialisation du Spinner //
        initSpinner();

        // Initialisation des CheckBoxes
        initCheckboxes();

        // Par défaut, bouton Save désactive avant modif. //
        // Bouton Annuler non clicable avant modif. //
        save_button.setEnabled(false);
        cancel_button.setClickable(false);

        Log.d(TAG,"instanciation du Fragment Configuration correctement effectuée");
        return rootView;
    }

    private void initView(View rootView){
        Log.i(TAG,"initialisation de la vue");
        sample_rate = (Spinner) rootView.findViewById(R.id.rate_spinner);
        cancel_button = (Button) rootView.findViewById(R.id.btn_cancel);
        save_button = (Button) rootView.findViewById(R.id.btn_save);
        EMGCheck = (CheckBox) rootView.findViewById(R.id.channel_1);
        EDACheck = (CheckBox) rootView.findViewById(R.id.channel_2);
        ECGCheck = (CheckBox) rootView.findViewById(R.id.channel_3);
        ACCCheck = (CheckBox) rootView.findViewById(R.id.channel_4);
        LUXCheck = (CheckBox) rootView.findViewById(R.id.channel_5);
        ABATCheck = (CheckBox) rootView.findViewById(R.id.channel_6);
    }

    private void initConf(){
        Log.i(TAG,"récupération des SharedPreferences");
        // Récupération des SharedPref de la MainActivity //
        rateValue = MainActivity.preferences.getInt(MainActivity.RATE_TAG, 1000);
        emg_state = MainActivity.preferences.getBoolean(MainActivity.EMG_TAG, true);
        eda_state = MainActivity.preferences.getBoolean(MainActivity.EDA_TAG, true);
        ecg_state = MainActivity.preferences.getBoolean(MainActivity.ECG_TAG, true);
        acc_state = MainActivity.preferences.getBoolean(MainActivity.ACC_TAG, true);
        lux_state = MainActivity.preferences.getBoolean(MainActivity.LUX_TAG, true);
        abat_state = MainActivity.preferences.getBoolean(MainActivity.ABAT_TAG, true);
    }

    private void initSpinner(){
        Log.i(TAG,"initialisation du spinner");
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.rate_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sample_rate.setAdapter(adapter);
        setSilentRatePosition(rateValue);
    }

    private void initCheckboxes(){
        Log.i(TAG,"initialisation des checkboxes");
        EMGCheck.setChecked(emg_state);
        EDACheck.setChecked(eda_state);
        ECGCheck.setChecked(ecg_state);
        ACCCheck.setChecked(acc_state);
        LUXCheck.setChecked(lux_state);
        ABATCheck.setChecked(abat_state);
    }

    private void initListeners(){
        Log.i(TAG,"initialisation des listeners");
        // Listener Spinner //
        sample_rate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                int rate_length = sample_rate.getSelectedItem().toString().length() - 3;
                rateValue = Integer.valueOf(sample_rate.getSelectedItem().toString().substring(0, rate_length));
                if (rateValue != MainActivity.preferences.getInt(MainActivity.RATE_TAG, 1000)){
                    switchSave();
                    switchCancel();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}

        });
        // Listeners Boutons //
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"sauvegarde des SharedPreferences");
                save_button.setEnabled(false);
                cancel_button.setClickable(false);
                SharedPreferences.Editor editor = MainActivity.preferences.edit();
                editor.putInt(MainActivity.RATE_TAG, rateValue);
                editor.putBoolean(MainActivity.EMG_TAG, emg_state);
                editor.putBoolean(MainActivity.EDA_TAG, eda_state);
                editor.putBoolean(MainActivity.ECG_TAG, ecg_state);
                editor.putBoolean(MainActivity.ACC_TAG, acc_state);
                editor.putBoolean(MainActivity.LUX_TAG, lux_state);
                editor.putBoolean(MainActivity.ABAT_TAG, abat_state);
                editor.commit();
                Toast.makeText(MainActivity.context, "Paramètres correctement sauvegardés", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Sauvegarde OK");
            }
        });
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"annulation des modifications");
                save_button.setEnabled(false);
                cancel_button.setClickable(false);
                // Reset des Valeurs //
                EMGCheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.EMG_TAG, true));
                EDACheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.EDA_TAG, true));
                ECGCheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.ECG_TAG, true));
                ACCCheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.ACC_TAG, true));
                LUXCheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.LUX_TAG, true));
                ABATCheck.setChecked(MainActivity.preferences.getBoolean(MainActivity.ABAT_TAG, true));
                setSilentRatePosition(MainActivity.preferences.getInt(MainActivity.RATE_TAG, 1000));
                Toast.makeText(MainActivity.context, "Paramètres précédents réstaurés", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Annulation OK");
            }
        });
        // Listeners CheckBoxes //
        EMGCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emg_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        EDACheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eda_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        ECGCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ecg_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        ECGCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ecg_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        ACCCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acc_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        LUXCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lux_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
        ABATCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abat_state = ((CheckBox) view).isChecked();
                switchSave();
                switchCancel();
            }
        });
    }

    private void switchSave(){
        if(!save_button.isEnabled()) {
            save_button.setEnabled(true);
        }
    }

    private void switchCancel(){
        if(!cancel_button.isClickable()){
            cancel_button.setClickable(true);
        }
    }

    private void setSilentRatePosition(Integer rateValue){
        switch (rateValue) {
            case 1000:
                sample_rate.setSelection(0, false);
                break;
            case 100:
                sample_rate.setSelection(1, false);
                break;
            case 10:
                sample_rate.setSelection(2, false);
                break;
            case 1:
                sample_rate.setSelection(3, false);
                break;
        }
    }
}