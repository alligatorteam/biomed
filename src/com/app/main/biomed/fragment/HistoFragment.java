package com.app.main.biomed.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.app.main.biomed.R;
import com.app.main.biomed.dialogs.HistoryDialog;
import com.app.main.biomed.list.FileItem;
import com.app.main.biomed.list.FileListAdapter;
import com.app.main.biomed.util.FileHelper;

import java.util.ArrayList;

public class HistoFragment extends ListFragment {
    public static FragmentManager fragmentManager;
    public HistoFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_histo, container, false);

        HistoFragment.fragmentManager = getFragmentManager();

        populateList(FileHelper.getAllFiles());
        return rootView;
    }

    private void populateList(ArrayList<FileItem> items) {
        FileListAdapter listAdapter = new FileListAdapter(this.getActivity(), R.layout.list_item_file, items);
        this.setListAdapter(listAdapter);
    }
}
