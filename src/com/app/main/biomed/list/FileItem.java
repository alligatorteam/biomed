package com.app.main.biomed.list;

import java.util.ArrayList;

/**
 * Created by gs309376 on 14/05/2014.
 */
public class FileItem {
    private String fileName, name;

    public FileItem(String fileName, String name) {
        this.fileName = fileName;
        this.name = name;
    }

    public String getName() { return this.name; }
    public String getFileName() { return this.fileName; }
}
