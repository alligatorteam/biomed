package com.app.main.biomed.list;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.main.biomed.R;
import com.app.main.biomed.dialogs.HistoryDialog;
import com.app.main.biomed.fragment.HistoFragment;
import com.app.main.biomed.util.FileHelper;

import java.util.List;

/**
 * Created by gs309376 on 14/05/2014.
 */
public class FileListAdapter extends ArrayAdapter<FileItem> {
    private List<FileItem> items;

    public FileListAdapter(Context context, int ressource, List<FileItem> items) {
        super(context, ressource, items);
        this.items = items;
    }

    private static class ViewHolder {
        public TextView name;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            // Création de la vue (qui sera ensuite réutilisée)
            v = LayoutInflater.from(getContext()).inflate(R.layout.list_item_file, parent, false);

            // Création du ViewHolder contenant les références vers les vues
            holder = new ViewHolder();
            holder.name = (TextView) v.findViewById(R.id.name);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final FileItem item = items.get(position);
        if (item != null) {
            holder.name.setText(item.getName());
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("file", item.getFileName());

                HistoryDialog historyDialog = new HistoryDialog();
                historyDialog.setArguments(args);
                historyDialog.show(HistoFragment.fragmentManager, "bla");
            }
        });
        return v;
    }
}
