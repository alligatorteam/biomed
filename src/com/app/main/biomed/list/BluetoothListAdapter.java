package com.app.main.biomed.list;

import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.app.main.biomed.MainActivity;
import com.app.main.biomed.R;
import com.app.main.biomed.fragment.AcquiFragment;
import com.app.main.biomed.fragment.ConnexionFragment;

import java.util.ArrayList;

/**
 * Created by gs309376 on 14/05/2014.
 */
public class BluetoothListAdapter extends ArrayAdapter<BluetoothDevice> {

    private ArrayList<BluetoothDevice> devices;

    public BluetoothListAdapter(Context context, int resource, ArrayList<BluetoothDevice> devices) {
        super(context, resource, devices);
        this.devices = devices;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView address;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            // Création de la vue (qui sera ensuite réutilisée)
            v = LayoutInflater.from(getContext()).inflate(R.layout.list_item_bluetooth, parent, false);

            // Création du ViewHolder contenant les références vers les vues
            holder = new ViewHolder();
            holder.name = (TextView) v.findViewById(R.id.name);
            holder.address = (TextView) v.findViewById(R.id.address);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final BluetoothDevice device = devices.get(position);
        if (device != null) {
            holder.name.setText(device.getName());
            holder.address.setText(device.getAddress());
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = MainActivity.preferences.edit();
                editor.putString("device", device.getAddress());
                editor.commit();

                Toast.makeText(MainActivity.context, "Choix de l'appareil sauvegardé", Toast.LENGTH_LONG).show();

                Fragment acquisition = new AcquiFragment();
                FragmentManager f = ConnexionFragment.fragmentManager;
                f.beginTransaction().replace(R.id.frame_container, acquisition).commit();
            }
        });
        return v;
    }
}