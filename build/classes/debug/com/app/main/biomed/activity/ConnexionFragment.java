package com.app.main.biomed.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.main.biomed.R;

public class ConnexionFragment extends Fragment {

    public ConnexionFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_connexion, container, false);

        return rootView;
    }
}
